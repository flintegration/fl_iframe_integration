/**
 * Copyright (c) Yodlee, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
 const path = require('path');
 const express = require('express');
 const exphbs = require('express-handlebars');
 const bodyParser = require('body-parser');
 const request = require('request');
 const https = require("https");
 const http = require("http");
 const fs = require('fs');
 let app = express();
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({extended: false}));
 let httpport = 9080;
 let httpsport = 9443;
 let error = false;
 let session = {};
 app.get('/', (request, response) => {
     console.log("--=== Index Default Router ===--");
     response.redirect('FL4/login');
 });
app.get('/FL3' || '/fl3', (request, response) => {
    console.log("--=== Index Default Router ===--");
    response.redirect('FL3/login');
});
app.get('/FL4' || '/fl4', (request, response) => {
    console.log("--=== Index Default Router ===--");
    response.redirect('FL4/login');
});
app.get('/FL3/login' || '/fl3/login', (request, response) => {
    console.log("--=== Index Default Router ===--");
    response.render('FL3/login', {layout: false});
});
app.get('/FL4/login' || '/fl4/login', (request, response) => {
    console.log("--=== Index Default Router ===--");
    response.render('FL4/login', {layout: false});
});
 app.post('/FL4/home', (req1, resp1) => {
     session.accessToken = req1.body.accesstoken;
     session.flurl = req1.body.flurl;
     session.extraparams = req1.body.extraparams;
     if (!session.accessToken || !session.flurl ) {
         resp1.redirect('/FL4/login');
         return;
     }
     if (session.accessToken.indexOf('Bearer') === -1) {
         session.accessToken = "Bearer " + session.accessToken
     }
     resp1.render('FL4/home', {
         layout: false,
         fastLinkURL: session.flurl,
         accessToken: session.accessToken,
         extraParams: JSON.stringify(session.extraparams)
     });
 });

app.post('/FL3/home', (req1, resp1) => {
    session.accessToken = req1.body.accesstoken;
    session.flurl = req1.body.flurl;
    session.extraparams = req1.body.extraparams;
    if (!session.accessToken || !session.flurl ) {
        resp1.redirect('/FL3/login');
        return;
    }
    if (session.accessToken.indexOf('Bearer') === -1) {
        session.accessToken = "Bearer " + session.accessToken
    }
    resp1.render('FL3/home', {
        layout: false,
        fastLinkURL: session.flurl,
        accessToken: session.accessToken,
        extraParams: JSON.stringify(session.extraparams)
    });
});
 if (!error) {
     var options = {
         key: fs.readFileSync('./views/keys/key.pem'),
         cert: fs.readFileSync('./views/keys/cert1.pem')
     };
     http.createServer(app).listen(httpport, function (req, res) {
        console.log("http:// Server is running at port "+httpport);
      });
     https.createServer(options, app).listen(httpsport, function (req, res) {
        console.log("https:// Server is running at port "+httpsport);
      });
 }
 app.engine('.hbs', exphbs({}));
 app.set('view engine', '.hbs');
 app.set('views', path.join(__dirname, 'views'));
 app.use(express.static('public'));
